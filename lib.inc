section .text


; Принимает код возврата и завершает текущий процесс
exit:
	mov rax, 60  						
	syscall 							 
	ret

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
        xor rax, rax						
    .loop:
        cmp byte [rdi + rax], 0				
        je .end								
        inc rax								
        jmp .loop							 
    .end:
        ret									

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:	
    xor rax, rax							
	call string_length
	mov rsi, rdi				
	mov rdx, rax						
	mov rax, 1							
	mov rdi, 1							
	syscall								
	ret								

; Переводит строку (выводить символ с кодом 0xA)
print_newline:
	mov rdi, 0xA
; Принимает код символа и выводит его в stdout
print_char:
	push rdi							
	mov rdi, rsp						
	call print_string					 
	pop rdi								
	ret

; Выводит беззнаковое 8-байтовое число в десятичном формате
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    xor rax, rax
        mov r10, 10
        mov r9, 0
	push r9 ; кладем нуль-терминатор в стек 
        mov rax, rdi
        .loop:
            xor rdx, rdx
            div r10
            mov rsi, rdx
            add rsi, 48
            inc r9
            dec rsp
            mov [rsp], sil
            cmp rax, 0
            jne .loop

        mov rdi, rsp ; выводим число
	call print_string

        add rsp, r9
	add rsp, 8 ; возвращаем стек в исходное состояние
        ret					

; Выводит знаковое 8-байтовое число в десятичном формате
print_int: 
	cmp rdi, 0 							; if(x < 0) goto .minus 
	jge print_uint							; 
	push rdi 							;
	mov rdi, '-' 						; если x < 0 то сначала выводим '-'
	call print_char 					;
	pop rdi 							; 
	neg rdi 							; а затем выводим (-x) как uint
	jmp print_uint

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
	xor rcx, rcx
	xor rax, rax
    .loop:
    	mov r8b, byte[rdi + rcx]
    	cmp r8b, byte[rsi + rcx]
    	jne .isNotEqual
    	inc rcx
    	cmp r8b, 0
    	je .isEqual
    	jmp .loop
    .isEqual:
    	mov rax, 1
    	ret
    .isNotEqual:
    	mov rax, 0
    	ret			

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
	xor rax, rax 						
	xor rdi, rdi 						
	push 0 								
	mov rsi, rsp 						
	mov rdx, 1 							
	syscall 							
	pop rax 							
	ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
	xor r9, r9 							
	mov r10, rdi 						
.loop:
	push rsi 							
	push rdi							
	call read_char 						
	pop rdi 							
	pop rsi 							
	test r9, r9 						
	je .spaces_check 						 
.sym:
	cmp r9, rsi 						
	jae .err 							
	cmp rax, ' ' 						
	je .add_null 						
	cmp rax, '	' 						
	je .add_null 						 
	cmp rax, '\n' 						
	je .add_null 						
	mov [rdi + r9], rax 				
	test rax, rax 						 
	je .end 							
	inc r9 								
	jmp .loop 							

.spaces_check:
	cmp rax, ' ' 						
	je .loop 							
	cmp rax, '	' 						
	je .loop 							
	cmp rax, '\n' 						
	je .loop 							
	jmp .sym 							

.add_null:
	xor rax, rax 						
	mov [rdi + r9], rax 				 
	jmp .end 							

.err:
	xor rax, rax 						
	ret 								
.end: 
	mov rax, r10 						
	mov rdx, r9 						
	ret 								

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
	mov r8, 10  						
	xor rax, rax 						
	xor rcx, rcx 						
.loop:
	movzx r9, byte [rdi + rcx] 			
	cmp r9b, '0' 						
	jb .end 							
	cmp r9b, '9' 						
	ja .end 							
	xor rdx, rdx 						
	mul r8							 
	sub r9b, '0' 						
	add rax, r9 						
	inc rcx								
	jmp .loop 							
.end:
	mov rdx, rcx 						
	ret

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был)
; rdx = 0 если число прочитать не удалось
parse_int:
	mov r8, 10 							
	xor rax, rax 						
	xor rcx, rcx 						
	push 1 								
.loop:
	movzx r9, byte [rdi + rcx] 			
	cmp r9b, '-' 						
	je .minus 							
	cmp r9b, '0' 						
	jb .check 							
	cmp r9b, '9' 						
	ja .check 							
	xor rdx, rdx 						
	mul r8 								
	sub r9b, '0' 						
	add rax, r9 						
.continue:
	inc rcx 							
	jmp .loop 							

.minus:
	mov r10, 0 							
	mov [rsp], r10 						 
	jmp .continue 						 

.check:
	pop r10 							
	cmp r10, 0 							 
	je .neg 							
	jmp .end 							

.neg:
	neg rax 							
	jmp .end 							

.end:
	mov rdx, rcx 						
	ret 								

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
 call string_length
    inc rax
    cmp rdx, rax
    jb .error
    xor rax, rax
    .loop:
        mov dl, byte[rdi+rax]
        mov byte[rsi+rax], dl
        inc rax
        cmp dl, 0
        jne .loop
    ret
    .error:
        xor rax, rax
        ret
